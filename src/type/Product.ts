type Product = {
    id: number
    IngredientID: string
    IngredientName: string
    Minimum: number
    Balance: number
    Price: number
}

export type {Product}