type Salary = {
    id: number;
    date: String;
    userid: number;
    workinghour: number;
    workrate: number;
}
export type { Salary }