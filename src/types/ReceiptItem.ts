import type { Product } from './Product'

type ReceiptItem = {
  id: number
  name: string
  price: number
  unit: number
  level?: Array<string>
  productId: number
  product?: Product
}

export { type ReceiptItem }
