type Product = {
  id: number
  name: string
  price: number
  level?: Array<string>
  type: number
}

export { type Product }
