import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const users: User[] = [
    {
      id: 0,
      email: 'User Name',
      password: '-',
      fullname: 'Name',
      gender: '-',
      roles: ['-']
    },
    {
      id: 1,
      email: 'mana123@gmail.com',
      password: 'Pass@1234',
      fullname: 'มานะ งานดี',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 2,
      email: 'duangjai123@gmail.com',
      password: 'Pass@4321',
      fullname: 'ดวงใจ อำพันธ์',
      gender: 'Female',
      roles: ['user']
    }
  ];
  
const currentUser = ref<User>(users[0]);
const login = (email: string, password: string): boolean => {
  
  const user = users.find(u => u.email === email && u.password === password);

  if (user) {
    
    currentUser.value = user;
    return true;
  }

  
  return false;
};

const logout = () => {
  
  currentUser.value = users[0];
};

const getUserById = (userId: number): User | undefined => {
 
  return users.find(u => u.id === userId);
};

return { currentUser, login, logout, getUserById };
});
