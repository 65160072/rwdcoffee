import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import ingredientService from '@/services/ingredientService'
import type { Ingredient } from '@/types/Ingredient'

const loadingStore = useLoadingStore()
const ingredients = ref<Ingredient[]>([])

export const useIngredientStore = defineStore('ingredient', () => {
  const initialIngredient: Ingredient = {
    IngredientID: '',
    IngredientName: '',
    Minimum: 0,
    Balance: 0,
    Price: 0
  }
  const editedIngredient = ref<Ingredient>(JSON.parse(JSON.stringify(initialIngredient)))

  async function getIngredients() {
    loadingStore.doLoad()
    const res = await ingredientService.getIngredients()
    ingredients.value = res.data
    loadingStore.finish()
  }

  async function getIngredient(id: number) {
    loadingStore.doLoad()
    const res = await ingredientService.getIngredient(id)
    editedIngredient.value = res.data
    loadingStore.finish()
  }

  async function saveIngredient() {
    const user = editedIngredient.value
    loadingStore.doLoad()
    if (!user.id) {
      //Add new
      const res = await ingredientService.addIngredient(user)
    } else {
      //Update
      const res = await ingredientService.updateIngredient(user)
    }
    await getIngredients()
    loadingStore.finish()
  }

  async function deleteIngredient() {
    loadingStore.doLoad()
    const res = await ingredientService.delIngredient(editedIngredient.value)
    await getIngredients()
    loadingStore.finish()
  }

  function clearForm() {
    editedIngredient.value = JSON.parse(JSON.stringify(initialIngredient))
  }
  return {
    ingredients,
    getIngredients,
    saveIngredient,
    deleteIngredient,
    clearForm,
    editedIngredient,
    getIngredient
  }
})
